Project 6: Brevet time calculator service
Simple listing service from project 5 stored in MongoDB database.

Author: Qi Han     Email: qhan@uoregon.edu

Project Description:

Reuse the app from project5, add functionalities as required:
   1). Change the values for host and port according to the machine, and use the web browser to check the results.
   2). Design RESTful service to expose what is stored in MongoDB. Specifically, you'll use the boilerplate given in DockerRestAPI folder, and 
   3). Create the three basic APIs and make them with following functions:
       "http://<host:port>/listAll" should return all open and close times in the database
       "http://<host:port>/listOpenOnly" should return open times only
       "http://<host:port>/listCloseOnly" should return close times only

       "http://<host:port>/listAll/csv" should return all open and close times in CSV format
       "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
       "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

       "http://<host:port>/listAll/json" should return all open and close times in JSON format

       "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
       "http://<host:port>/listCloseOnly/json" should return close times only in JSON format
   4). Add a query parameter to get top "k" open and close times.

