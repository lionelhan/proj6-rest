"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    # check the input
    if control_dist_km < 0:
      message = "invalid_input"
    elif 0 <= control_dist_km < brevet_dist_km:
      message = "continue"
    else:
      message = "DONE"

    brevet_start_time = arrow.get(brevet_start_time)
    # set calculation rules
    if brevet_dist_km <= control_dist_km <= brevet_dist_km * 1.2:
      if brevet_dist_km == 200: 
        opentime = brevet_start_time.shift(hours = (200 / 34))
      if brevet_dist_km == 300: 
        opentime = brevet_start_time.shift(hours = (200 / 34 + 100 / 32))
      if brevet_dist_km == 400: 
        opentime = brevet_start_time.shift(hours = (200 / 34 + 200 / 32))
      if brevet_dist_km == 600: 
        opentime = brevet_start_time.shift(hours = (200 / 34 + 200 / 32 + 200 / 30))
      if brevet_dist_km == 1000: 
        opentime = brevet_start_time.shift(hours = (200 / 34 + 200 / 32 + 200 / 30 + 400 / 28))
    # compute time
    else: 
      if control_dist_km <= 200:
        opentime = brevet_start_time.shift(hours = (control_dist_km / 34))

      elif 200 < control_dist_km <= 400:
        opentime = brevet_start_time.shift(hours = (200 / 34 + (control_dist_km - 200) / 32))

      elif 400 < control_dist_km <= 600:
        opentime = brevet_start_time.shift(hours = (200 / 34 + 200 / 32 + (control_dist_km - 400) / 30))

      elif 600 < control_dist_km < 1000:
        opentime = brevet_start_time.shift(hours = (200 / 34 + 200 / 32 + 200/ 30 + (control_dist_km - 600) / 28))

      # else:
      #   # control_dist_km over 1000
      #   opentime = (200 / 34 + 200 / 32 + 200/ 30 + (control_dist_km - 600) / 28)
    opentime = appro_time(opentime)
    return opentime.isoformat(), message


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start_time = arrow.get(brevet_start_time)
    if brevet_dist_km <= control_dist_km <= brevet_dist_km * 1.2:
      if brevet_dist_km == 200: 
        closetime = brevet_start_time.shift(hours = (200 / 15))
      if brevet_dist_km == 300: 
        closetime = brevet_start_time.shift(hours = (300 / 15))
      if brevet_dist_km == 400: 
        closetime = brevet_start_time.shift(hours = (400 /15))
      if brevet_dist_km == 600: 
        closetime = brevet_start_time.shift(hours = (600 / 15))
      if brevet_dist_km == 1000: 
        closetime = brevet_start_time.shift(hours = (600 / 15 + 400 / 11.428))
    elif control_dist_km == 0:
      closetime = brevet_start_time.shift(hours = 1)
    else: 
      if control_dist_km <= 600:
        # compute time
        closetime = brevet_start_time.shift(hours = (control_dist_km / 15))
      elif 600 < control_dist_km < 1000:
        closetime = brevet_start_time.shift(hours = (600 / 15 + (control_dist_km - 600) / 11.428))
    
    closetime = appro_time(closetime)
    return closetime.isoformat()

def appro_time(time):
  """
  Args: 
    time: a arrow time uses to be appropriate, through seconds away
  Return:
    appropriated time
  """
  if time.second >= 30:
    time = time.shift(minutes = 1)
  return time














