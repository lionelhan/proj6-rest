"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, render_template, Response
from flask_restful import Resource, Api
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.caldb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
############### 

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    # receive datas from AJAX
    km = request.args.get('km', 999, type=float)
    miles = request.args.get('miles', 999, type=float)
    brevet_km = request.args.get('brevet_dist_km', 999, type=int)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    # format the arrow time
    datetime = begin_date + ' ' + begin_time

    # set timezone
    start_date_time = arrow.get(datetime).replace(tzinfo='US/Pacific')
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # package and send data back to AJAX
    open_time, message = acp_times.open_time(km, brevet_km, start_date_time.isoformat())
    close_time = acp_times.close_time(km, brevet_km, start_date_time.isoformat())
    result = {"open": open_time, "close": close_time, "message": message}
    return flask.jsonify(result=result)

@app.route("/_result")
def _result():
    _items = db.caldb.find()
    items = [item for item in _items]
    _distance = db.distance.find()
    distance = [distance for distance in _distance]
    print("inside the _display")
    return render_template('result.html', items=items, distance=distance)
@app.route("/_display")
def _display():
    if db.caldb.count() == 0:
        res = "False"
    else:
        res = "True"
    result = {"res": res}
    return flask.jsonify(result=result)
@app.route("/_submit")
def _submit():
    myargs = request.args.get('res')
    print(myargs == "")
    if myargs == "":
        res = "False"
    else:
        res = "True"
        db.caldb.drop()
        db.distance.drop()
        distance = request.args.get('distance', 999, type=int)
        dict_distance = {}
        dict_distance['distance'] = distance
        db.distance.insert_one(dict_distance)
        array = myargs.split(',')
        array_length = len(array)
        pair_num = int(array_length/4)
        counter = 0
        for index in range(pair_num):
            dic = {}
            dic['mile'] = array[counter]
            counter += 1
            dic['km'] = array[counter]
            counter += 1
            dic['opentime'] = array[counter]
            counter += 1
            dic['closetime'] = array[counter]
            counter += 1
            db.caldb.insert_one(dic)
    result = {"res": res}
    return flask.jsonify(result=result)

def get_data():
	_items = db.caldb.find()
	data = [item for item in _items] 
	return data
	
	
class listAll(Resource):
	def get(self):
		data = get_data()
		opentime = [item['opentime'] for item in data]
		closetime = [item['closetime'] for item in data]
		return {'Opentime':opentime, 'Closetime':closetime}
		
		
class listOpenOnly(Resource):
	def get(self):
		data = get_data()
		opentime = [item['opentime'] for item in data]
		if 'top' not in request.args:	
			return {'Opentime':opentime}
		request_top = request.args.get("top", 0, type=int)
		open_top = []
		counter = request_top
		for item in opentime:
			if counter == 0:
				break
			open_top.append(item)
			counter -= 1
		return {'Opentime':open_top}
		
		
class listCloseOnly(Resource):
	def get(self):
		data = get_data()
		closetime = [item['closetime'] for item in data]
		if 'top' not in request.args:	
			return {'Closetime':closetime}
		request_top = request.args.get("top", 0, type=int)
		close_top = []
		counter = request_top
		for item in closetime:
			if counter == 0:
				break
			close_top.append(item)
			counter -= 1
		return {'Closetime':close_top}
		
		
class listAllCSV(Resource):
	def get(self):
		data = get_data()
		output = StringIO("")
		filedname = ["opentime", "closetime"]
		writer = csv.DictWriter(output, filedname, extrasaction='ignore')
		writer.writeheader()
		if 'top' not in request.args:
			for item in data:
				writer.writerow(item)
		request_top = request.args.get("top", 0, type=int)
		counter = request_top
		for item in data:
			if counter == 0:
				break
			else:
				writer.writerow(item)
				counter -= 1
		content = output.getvalue()
		response = Response(content)
		response.mimetype = "text/csv"
		response.headers['content-Disposition'] = "inline"
		return response
		
		
class listOpenOnlyCSV(Resource):
	def get(self):
		data = get_data()
		output = StringIO("")
		filedname = ["opentime"]
		writer = csv.DictWriter(output, filedname, extrasaction='ignore')
		writer.writeheader()
		if 'top' not in request.args:
			for item in data:
				writer.writerow(item)
		request_top = request.args.get("top", 0, type=int)
		counter = request_top
		for item in data:
			if counter == 0:
				break
			else:
				writer.writerow(item)
				counter -= 1
		content = output.getvalue()
		response = Response(content)
		response.mimetype = "text/csv"
		response.headers['content-Disposition'] = "inline"
		return response



class listCloseOnlyCSV(Resource):
	def get(self):
		data = get_data()
		output = StringIO("")
		filedname = ["closetime"]
		writer = csv.DictWriter(output, filedname, extrasaction='ignore')
		writer.writeheader()
		if 'top' not in request.args:
			for item in data:
				writer.writerow(item)
		request_top = request.args.get("top", 0, type=int)
		counter = request_top
		for item in data:
			if counter == 0:
				break
			else:
				writer.writerow(item)
				counter -= 1
		content = output.getvalue()
		response = Response(content)
		response.mimetype = "text/csv"
		response.headers['content-Disposition'] = "inline"
		return response


# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json', "/")
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')

#############
app.secret_key = 'Try one more time'

if __name__ == "__main__":

    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=80, host="0.0.0.0", debug=True)
