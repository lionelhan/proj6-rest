# Laptop Service

from flask import Flask
from flask_restful import Resource, Api
import os
import csv
from pymongo import MongoClient
from io import StringIO

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.caldb

def get_data():
	_items = db.caldb.find()
	data = [item for item in _items] 
	return data
	
	
class listAllJSON(Resource):
	def get(self):
		data = get_data()
		opentime = [item['opentime'] for item in data]
		closetime = [item['closetime'] for item in data]
		return {'Opentime':opentime, 'Closetime':closetime}
		
		
class listOpenOnlyJSON(Resource):
	def get(self):
		data = get_data()
		opentime = [item['opentime'] for item in data]
		if 'top' not in request.args:	
			return {'Opentime':opentime}
		request_top = request.args.get("top", 0, type=int)
		open_top = []
		counter = request_top
		for item in opentime:
			if counter == 0:
				break
			open_top.append(item)
			counter -= 1
		return {'Opentime':open_top}
		
		
class listCloseOnlyJSON(Resource):
	def get(self):
		data = get_data()
		closetime = [item['closetime'] for item in data]
		if 'top' not in request.args:	
			return {'Closetime':closetime}
		request_top = request.args.get("top", 0, type=int)
		close_top = []
		counter = request_top
		for item in closetime:
			if counter == 0:
				break
			close_top.append(item)
			counter -= 1
		return {'Closetime':close_top}
		
		
class listAllCSV(Resource):
	def get(self):
		data = get_data()
		output = StringIO("")
		filedname = ["opentime", "closetime"]
		writer = csv.DictWriter(output, filedname, extrasaction='ignore')
		writer.writeheader()
		if 'top' not in request.args:
			for item in data:
				writer.writerow(item)
		request_top = request.args.get("top", 0, type=int)
		counter = request_top
		for item in data:
			if counter == 0:
				break
			else:
				writer.writerow(item)
				counter -= 1
		content = output.getvalue()
		response = Response(content)
		response.mimetype = "text/csv"
		response.headers['content-Disposition'] = "inline"
		return response
		
		
class listOpenOnlyCSV(Resource):
	def get(self):
		data = get_data()
		output = StringIO("")
		filedname = ["opentime"]
		writer = csv.DictWriter(output, filedname, extrasaction='ignore')
		writer.writeheader()
		if 'top' not in request.args:
			for item in data:
				writer.writerow(item)
		request_top = request.args.get("top", 0, type=int)
		counter = request_top
		for item in data:
			if counter == 0:
				break
			else:
				writer.writerow(item)
				counter -= 1
		content = output.getvalue()
		response = Response(content)
		response.mimetype = "text/csv"
		response.headers['content-Disposition'] = "inline"
		return response



class listCloseOnlyCSV(Resource):
	def get(self):
		data = get_data()
		output = StringIO("")
		filedname = ["closetime"]
		writer = csv.DictWriter(output, filedname, extrasaction='ignore')
		writer.writeheader()
		if 'top' not in request.args:
			for item in data:
				writer.writerow(item)
		request_top = request.args.get("top", 0, type=int)
		counter = request_top
		for item in data:
			if counter == 0:
				break
			else:
				writer.writerow(item)
				counter -= 1
		content = output.getvalue()
		response = Response(content)
		response.mimetype = "text/csv"
		response.headers['content-Disposition'] = "inline"
		return response


# Create routes
# Another way, without decorators
api.add_resource(listAllJSON, '/listAllJSON', '/listAll/json', "/")
api.add_resource(listOpenOnlyJSON, '/listOpenOnlyJSON', '/listOpenOnly/json')
api.add_resource(listCloseOnlyJSON, '/listCloseOnlyJSON', '/listCloseOnly/json')

api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
